﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellUIBehaviour : ElementBehaviour {

    //public GameObject ispell;

    // Use this for initialization
    void Awake()
    {
        Spell spell = new Spell();
        spell.index += GetIndex;
        this.element = spell;

    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void setElement(Element element)
    {
        this.element = element;
    }
    public override void putnStack()
    {
        if (GameManager.i.GetCurrentPlayerType() == 0 && GameManager.i.stackManager.spellsInStack.Count < 8)
        {
            if (inStack)
            {
                GameManager.i.RemoveSpell(this);
            }
            else
            {
                GameManager.i.PutSpell(this);
            }
        }
    }
    public override void Destroy()
    {
        //Destroy(ispell);
        Destroy(gameObject);
    }
    public override Element process()
    {
        GameManager.i.stackManager.spellsInStack.Remove(this);
        Destroy(gameObject);
        return element;
    }
    public override float GetHeight()
    {
        return GetComponent<RectTransform>().rect.height;
    }
    public override float GetWidth()
    {
        return GetComponent<RectTransform>().rect.width;
    }

    public override void displayElement()
    {

    }
    public byte GetIndex(){
        return index;
    }

    public class Spell : Element
    {
        public delegate byte Index();
        public Index index;

        public override byte GetCriptoGrama()
        {
            return index();
        }

        public override void SetCriptoGram(byte criptogram)
        {
            throw new System.NotImplementedException();
        }
    }
}
