﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : GameManager
{

    [SerializeField]
    TimerBehaviour timer;

    [SerializeField]
    TutorialBehaviour[] tutorials;

    [SerializeField]
    string[] messages;


    public Player[] players;

    int playersTurn = 0;

    int tutorialIndex;

    bool inProgress;

    public override int GetCurrentPlayerType()
    {
        return 0;
    }

    public override void Init()
    {
        bgManager.SetBackground(-1);
        PlayerPrefs.SetInt("GameType", 0);
        for (int i = 0; i < players.Length; i++)
        {
            int index = 0;
            if (i == 0)
            {
                index = 0;
            }
            else
            {
                index = 7;
            }
            players[i].InstantitePlayer(i + 1, races[index],1);
            players[i].life = 100;
        }

        int magicIndex = GetMagicIndex(players[1].race.magic);
        bgManager.SetBackground(magicIndex);
        musicManager.SetMusic(magicIndex);
        players[playersTurn].SetTurn();
        DisplayLesson();
    }
    private int next()
    {
        return (playersTurn + 1) % 2;
    }
    public override void setTurnAction()
    {
        if (inProgress)
            return;

        Debug.Log("Turning...");
        Damage damage = players[0].MakeDamage(stackManager.resolveSpell());
        int magic = damage.magic;
        int strongest = players[0].race.strongest;
        int weakness = players[1].race.weakness;
        int afinity = players[1].race.afinity;
        comboManager.CheckCombo(magic, strongest, weakness, afinity, damage.amoung);
        //playersTurn = next();
        int life = players[1].TakeDamage(damage);
        players[1].playerManager.setLife(life);

        players[0].SetTurn();
        
        timer.ResetTimer();

        if (playMode == 0)
        {
            ClearRunes();
            ReplenishRunes(0, stackSize);
        }
        else
        {
            int start = stackManager.runeBehaviour.transform.childCount;
            int finish = stackSize - start;
            ArrangeRunes();
            ReplenishRunes(start, finish);
        }

        DisplayLesson();
    }
    private void DisplayLesson()
    {
        inProgress = true;
        timer.enabled = false;
        tutorials[tutorialIndex].gameObject.SetActive(true);
        tutorials[tutorialIndex].DisplayTutorial();
    }
    public void FinishLesson(){
        inProgress = false;
        tutorials[tutorialIndex].StartPractice();
        tutorials[tutorialIndex].gameObject.SetActive(false);
        timer.enabled = true;

        Debug.Log("finish lesson, next: " + tutorialIndex);
    }

    public override void PutRune(ElementBehaviour element)
    {
        if (!inProgress)
            stackManager.putRune(element);
    }

    public override void RemoveRune(ElementBehaviour element)
    {
        if (!inProgress)
            stackManager.removeRune(element);
    }

    public override void PutSpell(ElementBehaviour element)
    {
        if (!inProgress)
            stackManager.putSpell(element);
    }

    public override void RemoveSpell(ElementBehaviour element)
    {
        if (!inProgress)
            stackManager.removeSpell(element);
    }

    public override Player GetEnemyPlayer()
    {
        throw new System.NotImplementedException();
    }
    public override void ExitMatch()
    {
        sceneChanger.Previous(); ;
    }
}
