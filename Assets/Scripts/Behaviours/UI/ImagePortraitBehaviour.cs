﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ImagePortraitBehaviour : MonoBehaviour, IPointerClickHandler {
    [SerializeField]
    Race race;

    [SerializeField]
    Text description;

    [SerializeField]
    Text toConfirm;

    [SerializeField]
    int index;
    [SerializeField]
    bool isEnemy;

    [SerializeField]
    SoundManager sound;

    public void OnPointerClick(PointerEventData eventData)
    {
        sound.AcceptSound();
        description.text = race.description;
        toConfirm.text = "Confirm";
        if (!isEnemy)
        {
            PlayerPrefs.SetInt("Race", index);
            /*if (PlayerPrefs.GetInt("Level") == 0)
            {
                PlayerPrefs.SetInt("Level", race.level);
            }*/
        }else{
            PlayerPrefs.SetInt("Enemy",index);
        }
    }
}
