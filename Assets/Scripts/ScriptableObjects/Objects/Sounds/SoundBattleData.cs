﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundBattleData", menuName = "Objects/Sounds/SoundBattleData")]
public class SoundBattleData : SoundData
{

    public AudioClip CastSound()
    {
        return audios[0];
    }
    public AudioClip CrashSound()
    {
        return audios[1];
    }
    public AudioClip ComboSound()
    {
        return audios[2];
    }
    public AudioClip DiedMaleSound()
    {
        return audios[3];
    }
    public AudioClip DiedFemaleSound()
    {
        return audios[4];
    }
    public AudioClip DamageMaleSound()
    {
        return audios[5];
    }
    public AudioClip DamageFemaleSound()
    {
        return audios[6];
    }
}
