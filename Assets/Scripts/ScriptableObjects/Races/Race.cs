﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RaceModel", menuName = "Race")]
public class Race : ScriptableObject, IDamage
{
    [SerializeField]
    protected const int POWER = 4;
    public int genere;

    public Sprite sprite;

    public int level = 1;

    public byte magic = 0;
    public byte afinity = 0;
    public byte weakness = 0;
    public byte strongest = 0;

    public string name;

    public string description;

    public byte GetMagicAfinity()
    {
        return afinity;
    }

    public Damage MakeDamage(Damage damage)
    {
        //Debug.Log("* making damage.magic: " + damage.magic + " damage.amoung: " + damage.amoung);
        //Debug.Log("* strongest: " + strongest);
        if ((damage.magic & strongest) != 0)
        {
            damage.amoung *= POWER;
            //Debug.Log("* have strongest final damage: " + damage.amoung);
        }
        return damage;
    }

    public int TakeDamage(Damage damage)
    {
        //Debug.Log("+ taking damage.magic: " + damage.magic + " damage.amoung: " + damage.amoung);
        //Debug.Log("+ weakness: " + weakness);
        if ((damage.magic & weakness) != 0)
        {
            damage.amoung *= POWER;
            //Debug.Log("+ have weakness final damage: " + damage.amoung);
        }
        //Debug.Log("+ strongest: " + strongest);
        if ((damage.magic & strongest) != 0)
        {
            damage.amoung /= POWER;
            //Debug.Log("+ have strongest final damage: " + damage.amoung);
        }
        while ((damage.magic & afinity) != 0 && damage.amoung > 0)
        {
            damage.amoung -= POWER;
            //Debug.Log("+ have afinity " + afinity + " final damage: " + damage.amoung);
            afinity = EsmeraldTable.GetAfinity(afinity);
        }
        return damage.amoung;
    }
}
