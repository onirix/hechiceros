﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBehaviour : MonoBehaviour
{

    [SerializeField]
    float threshold;

    Text timerText;

    public float currentTime;

    private void Start()
    {
        timerText = GetComponent<Text>();
        currentTime = Time.time;
    }

    private void Update()
    {
        int time = (int)(Time.time - currentTime);
        if (time > threshold)
        {
            GameManager.i.setTurnAction();
            currentTime = Time.time;
        }
        timerText.text = "" + (threshold - time);
    }

    public void ResetTimer()
    {
        currentTime = Time.time;
    }
}
