﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class PlayerManager : MonoBehaviour
{

    protected LifeBehaviour playerLife;
    public int playerNumber;
    public bool haveTurn;

    public abstract void setTurn();
    public abstract void ResolveTurn();

    public abstract void setLife(int life);

    public void Display(Race race, int playerNumber, int level)
    {
        this.playerNumber = playerNumber;
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("player" + playerNumber);
        foreach (GameObject gameObject in gameObjects)
        {
            PortraitBehaviour portrait = gameObject.GetComponent<PortraitBehaviour>();
            if ((object)portrait != null)
            {
                portrait.DisplayPlayer(race, level);
                continue;
            }
            playerLife = gameObject.GetComponent<LifeBehaviour>();
        }
    }
}
