﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBehaviour : MonoBehaviour {
    [SerializeField]
    Text messageText;

    public void SetMessage(string message){
        messageText.text = message;
    }
}
