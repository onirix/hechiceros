﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboManager : MonoBehaviour {

    [SerializeField]
    Text messageText;

    Queue<string> messages = new Queue<string>();

    bool inQueue;

    public void CheckCombo(int magic, int strongest, int weakness, int afinity, int amoung)
    {
        if (amoung > 0 && (magic&afinity)==0) {
            byte i = 0;
            if ((magic & strongest) != 0)
            {
                PutMessage("Power Up!");
                i |= 1;
            }
            if ((magic & weakness) != 0)
            {
                PutMessage("Critical!");
                i |= 2;
            }
            if (i == 2)
            {
                PutMessage("Super Combo!!");
            }
        }
    }
    public void PutMessage(string message)
    {
        if (!inQueue)
        {
            inQueue = true;
            messageText.enabled = true; ;
            StartCoroutine(processMessage(message));
        }
        else
        {
            messages.Enqueue(message);
        }
    }
    private IEnumerator processMessage(string message)
    {
        messageText.text = message;
        yield return new WaitForSeconds(1);
        while (messages.Count > 0)
        {
            message = messages.Dequeue();
            yield return new WaitForSeconds(1);
        }
        inQueue = false;
        messageText.enabled = false;
    }

}
