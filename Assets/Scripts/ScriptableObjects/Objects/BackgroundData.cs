﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BackgroundData", menuName = "Objects/BackgroundData")]
public class BackgroundData : ScriptableObject {

    public Sprite[] sprites;

	public Sprite GetBackGround(int index)
    {
        return sprites[index];
    }
}
