﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneResultUIBehaviour : ElementResultBehaviour
{
    [SerializeField]
    Sprite[] spellSprite;

    [SerializeField]
    Sprite[] runeSprite;

    [SerializeField]
    GameObject ispellPrefab;

    [SerializeField]
    GameObject runeImagePrefab;

    [SerializeField]
    Transform spellContainer;

    [SerializeField]
    Transform runeContainer;

    [SerializeField]
    RectTransform runeContainer2;

    [SerializeField]
    Text number;



    private void Awake()
    {
    }


    public override void Destroy()
    {
        ClearContainer(runeContainer);
        ClearContainer(spellContainer);
        ClearContainer(runeContainer2);
    }
    public void ClearContainer(Transform container)
    {
        for (int i = 0; i < container.childCount; i++)
        {
            Destroy(container.GetChild(i).gameObject);
        }
    }

    public override void displayElement()
    {
        Destroy();
        DisplayMagic();
        DisplaySpell();
    }
    public void Display(byte magic, Sprite[] sprites, Transform container)
    {
        int i = 0;
        while(magic != 0)
        {
            if ((magic & 1) == 1)
            {
                InstantiateElement(sprites[i], container);
            }
            magic = (byte)(magic >> 1);
            i++;
        }
    }
    public override void DisplayMagic()
    {
        ClearContainer(runeContainer);
        byte magic = element.GetCriptoGrama();
        float hoffset = runeContainer2.rect.width / 2f;
        number.text = "" + magic;
        int i = 0;
        while (magic != 0)
        {
            if ((magic & 1) == 1)
            {
                InstantiateElement(runeSprite[i], runeContainer);
                InstantiateRune2(i, hoffset);
            }
            magic = (byte) (magic >> 1);
            i++;
        }
        //Display(magic, runeSprite, runeContainer);
    }
    public override void DisplaySpell()
    {
        ClearContainer(spellContainer);
        Display(spellResult, spellSprite, spellContainer);
    }

    public override float GetHeight()
    {
        return 0;
    }

    public override float GetWidth()
    {
        return 0;
    }

    public override Element process()
    {
        GameManager.i.stackManager.runesInStack.Remove(this);
        Destroy(gameObject);
        return element;
    }

    public override void putnStack()
    {


    }
    private void InstantiateRune2(int i, float hoffset)
    {
        //float scaler = GameManager.i.scalefactor;
        GameObject rune = Instantiate(runeImagePrefab);
        //rune.transform.localScale = new Vector3(scaler,scaler, scaler);
        RuneDisplayBehaviour runeImage = rune.GetComponent<RuneDisplayBehaviour>();
        runeImage.SetRune(transform, i);
        RectTransform runeRectTransform = (RectTransform)rune.transform;
        runeRectTransform.SetParent(runeContainer2);
        rune.transform.localScale = Vector3.one;
        runeRectTransform.localPosition = new Vector3(runeRectTransform.rect.width * i - hoffset + runeRectTransform.rect.width / 2f, -10f, 0f);
    }
    private void InstantiateElement(Sprite sprite, Transform parent)
    {
        GameObject ispell = Instantiate(ispellPrefab);
        ispell.transform.SetParent(parent);
        ispell.transform.localScale = Vector3.one;
        ispell.transform.localPosition = Vector3.zero;
        Image ispellImage = ispell.GetComponent<Image>();
        ispellImage.sprite = sprite;
    }
    public override void AddMagicElement(Element element)
    {
        byte magic = element.GetCriptoGrama();
        Display(magic, runeSprite, runeContainer);
        this.element.SetCriptoGram((byte)(element.GetCriptoGrama() | magic));
    }

    public override void SetMagicElement(Element element)
    {
        byte magic = element.GetCriptoGrama();
        //Display(magic, runeSprite, runeContainer);
        this.element.SetCriptoGram(magic);
        DisplayMagic();
    }
    public override void setElement(Element element)
    {
        byte i = element.GetCriptoGrama();
        byte j = this.element.GetCriptoGrama();
        Debug.Log("i: " + i + " j: " + j);
        if (i == 0)
        {
            if ((j & 128) == 0)
            {
                this.element.SetCriptoGram((byte)(j | 128));
                InstantiateElement(runeSprite[0], runeContainer);
            }
        }
        else if ((i & j) == 0)
        {
            byte result = (byte)(j | i);
            this.element.SetCriptoGram(result);

            for (int l = 0; l < spellSprite.Length; l++)
            {
                if (((i >> l) & 1) == 1)
                {
                    InstantiateElement(runeSprite[l + 1], runeContainer);
                    return;
                }
            }
        }
    }

}
