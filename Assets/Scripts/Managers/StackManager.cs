﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackManager : MonoBehaviour
{
    public StackBehaviour runeStackBehaviour;
    public StackBehaviour runeBehaviour;
    public StackBehaviour spellStackBehaviour;

    public List<ElementBehaviour> runesInStack = new List<ElementBehaviour>();
    public List<ElementBehaviour> spellsInStack = new List<ElementBehaviour>();

    public ElementResultBehaviour runeResult;

    byte lastResult;

    public bool isTraining;


    public void startStack()
    {
        runeResult.element = new Rune(0);
    }

    public void putSpell(ElementBehaviour spell)
    {
        if (spellsInStack.Count > 6)
        {
            return;
        }
        ElementBehaviour newspell = Instantiate(spell.gameObject).GetComponent<ElementBehaviour>();
        spellsInStack.Add(newspell);
        spellStackBehaviour.updateStack(newspell);
        Debug.Log("Putting spell");
        if (!isTraining && runesInStack.Count > 0)
        {
            PreviewSpell();
        }
    }
    public void removeSpell(ElementBehaviour spell)
    {
        spellsInStack.Remove(spell);
        spellStackBehaviour.updateStack(spell);
        if (!isTraining && runesInStack.Count > 0)
        {
            PreviewSpell();
        }
    }

    public void putRune(ElementBehaviour rune)
    {
        runeStackBehaviour.updateStack(rune);
        runesInStack.Add(rune);
        if (!isTraining)
            PreviewSpell();
    }
    public void removeRune(ElementBehaviour rune)
    {
        runesInStack.Remove(rune);
        runeBehaviour.updateStack(rune);
        runeStackBehaviour.RefreshStack();
        if (!isTraining)
        {
            if (runesInStack.Count == 0)
            {
                runeResult.element.SetCriptoGram(0);
                runeResult.displayElement();
            }
            else
            {
                PreviewSpell();
            }
        }
    }
    public void PreviewSpell()
    {
        Damage damage = new Damage();
       
        runeResult.element.SetCriptoGram(runesInStack[0].element.GetCriptoGrama());
        if (runesInStack.Count == 1 && spellsInStack.Count > 0)
        {
            byte spell = spellsInStack[0].element.GetCriptoGrama();
            if (spell == 1)
            {
                Not();
            }
        }
        else {
            int j = 0;
            for (int i = 1; i < runesInStack.Count; i++)
            {
                Debug.Log("i: " + i);
                if (j < spellsInStack.Count)
                {
                    byte spell = spellsInStack[j].element.GetCriptoGrama();
                    Debug.Log(i+ "**************spell: " + spell);
                    if (spell == 1)
                    {
                        Not();
                        j++;
                        if (j >= spellsInStack.Count)
                            break;

                        spell = spellsInStack[j].element.GetCriptoGrama();
                    }
                    if (runesInStack.Count > 0)
                    {
                        ProcessSpell(spell, runesInStack[i].element.GetCriptoGrama());
                    }
                    Debug.Log(". r: " + runeResult.element.GetCriptoGrama());
                    j++;
                }
            }
        }
        runeResult.displayElement();
    }
    public Damage resolveSpell()
    {
        Damage damage = new Damage();
        if (runesInStack.Count == 0 && runeResult.element.GetCriptoGrama() == 0)
            return damage;

        //Debug.Log("+ r: " + runeResult.element.GetCriptoGrama());
        if (runeResult.element.GetCriptoGrama() == 0)
            runeResult.element.SetCriptoGram(runesInStack[0].process().GetCriptoGrama());

        //Debug.Log("+ r: " + runeResult.element.GetCriptoGrama());
        while (spellsInStack.Count > 0 && runesInStack.Count > 0)
        {
            byte spell = spellsInStack[0].process().GetCriptoGrama();
            if (spell == 1)
            {
                Not();
            }
            else if (runesInStack.Count > 0)
            {
                //Debug.Log("**************spell: " + spell);
                int plus = ProcessSpell(spell, runesInStack[0].process().GetCriptoGrama());
                if (plus > 0)
                {
                    Debug.Log("****************is combo!!!");

                    GameManager.i.soundManager.ComboSound();
                }
                else
                {

                    Debug.Log("****************no combo!!!");
                }
                damage.amoung += plus;
                //Debug.Log(". r: " + runeResult.element.GetCriptoGrama());
                damage.amoung++;
            }
        }
        spellsInStack.Clear();
        runesInStack.Clear();
        spellStackBehaviour.ClearStack();
        runeStackBehaviour.ClearStack();
        runeResult.spellResult = 0;
        runeResult.displayElement();
        damage.magic = runeResult.element.GetCriptoGrama();
        lastResult = damage.magic;
        return damage;
    }
    int ProcessSpell(byte spell, byte magic)
    {
        int PLUS = 1;
        switch (spell)
        {
            case 2://And
                //Debug.Log("************** AND: " + spell);
                And(magic);
                return PLUS;
            case 4://NAnd
                //Debug.Log("************** NAND: " + spell);
                And(magic);
                Not();
                return PLUS;
            case 8://Or
                //Debug.Log("************** OR: " + spell);
                Or(magic);
                break;
            case 16://NOr
                //Debug.Log("************** NOR: " + spell);
                Or(magic);
                Not();
                return PLUS;
            case 32://Xor
                //Debug.Log("************** XOR: " + spell);
                Xor(magic);
                return PLUS;
            case 64://NXor
                //Debug.Log("************** NXOR: " + spell);
                Xor(magic);
                Not();
                return PLUS;
        }
        return 0;
    }
    void Not()
    {
        runeResult.element.SetCriptoGram((byte)~runeResult.element.GetCriptoGrama());
    }
    void And(byte magic)
    {
        runeResult.element.SetCriptoGram((byte)(runeResult.element.GetCriptoGrama() & magic));
    }
    void Or(byte magic)
    {
        runeResult.element.SetCriptoGram((byte)(runeResult.element.GetCriptoGrama() | magic));
    }
    void Xor(byte magic)
    {
        runeResult.element.SetCriptoGram((byte)(runeResult.element.GetCriptoGrama() ^ magic));
    }

}


