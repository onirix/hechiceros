﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HumanPlayerManager : PlayerManager {

    CastBehaviour cast;

    private void Awake()
    {
        cast = GameObject.FindWithTag("cast").GetComponent<CastBehaviour>();
        cast.resolve += ResolveTurn;
    }

    public override void setTurn()
    {
        cast.SetEnable(true);
        haveTurn = true;
    }
    public override void ResolveTurn()
    {
        if (haveTurn)
        {
            cast.SetEnable(false);
            haveTurn = false;
            GameManager.i.setTurnAction();
        }
    }
    public override void setLife(int life)
    {
        playerLife.SetLife(life);
    }
}
