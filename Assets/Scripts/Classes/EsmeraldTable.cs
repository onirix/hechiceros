﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsmeraldTable
{

    public static byte GetWeakness(int magic)
    {
        byte weakness = 0;
        if ((magic & 1) == 1)
        {
            weakness |= 64;
        }
        if ((magic & 2) == 2)
        {
            weakness |= 4;
        }
        if ((magic & 4) == 4)
        {
            weakness |= 8;
        }
        if ((magic & 8) == 8)
        {
            weakness |= 16;
        }
        if ((magic & 16) == 16)
        {
            weakness |= 32;
        }
        if ((magic & 32) == 32)
        {
            weakness |= 128;
        }
        if ((magic & 64) == 64)
        {
            weakness |= 2;
        }
        if ((magic & 128) == 128)
        {
            weakness |= 1;
        }
        return 0;
    }
    public static byte GetStrongest(int magic)
    {
        byte strongest = 0;
        if ((magic & 1) == 1)
        {
            strongest |= 128;
        }
        if ((magic & 2) == 2)
        {
            strongest |= 64;
        }
        if ((magic & 4) == 4)
        {
            strongest |= 16;
        }
        if ((magic & 8) == 8)
        {
            strongest |= 4;
        }
        if ((magic & 16) == 16)
        {
            strongest |= 8;
        }
        if ((magic & 32) == 32)
        {
            strongest |= 2;
        }
        if ((magic & 128) == 128)
        {
            strongest |= 1;
        }
        return strongest;
    }
    public static byte GetAfinity(int magic)
    {
        byte afinity = 0;

        if ((magic & 1) == 1)
        {
            afinity |= 32;
        }
        if ((magic & 2) == 2)
        {
            afinity |= 1;
        }
        if ((magic & 4) == 4)
        {
            afinity |= 2;
        }
        if ((magic & 8) == 8)
        {
            afinity |= 128;
        }
        if ((magic & 16) == 16)
        {
            afinity |= 4;
        }
        if ((magic & 32) == 32)
        {
            afinity |= 16;
        }
        if ((magic & 64) == 64)
        {
            afinity |= 8;
        }
        if ((magic & 128) == 128)
        {
            afinity |= 64;
        }

        return afinity;
    }
}

/*
 * white:1 yellow:2 blue:4 green:8 red:16 gray:32 purple:64 black:128
 * 
 * white:1    *black:128    -purple:64    +gray:32
 * yellow:2   *purple:64    -blue:4       +white:1
 * blue:4     *red:16       -green:8      +yellow:2
 * green:8    *blue:4       -red:16       +black:128
 * red:16     *green:8      -gray:32      +blue:4
 * gray:32    *yellow:2     -black:128    +red:16
 * purple:64  *white:1      -yellow:2     +green:8
 * black:128  *gray:32      -white:1      +purple:64
 */
