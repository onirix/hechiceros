﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public abstract class GameManager : MonoBehaviour
{
    public BackgroundManager bgManager;
    public Race[] races;
    public static GameManager i;
    public SceneChanger sceneChanger;
    public StackManager stackManager;
    public DataManager dataManager;
    public SoundBattleManager soundManager;
    public MusicManager musicManager;
    public ElementBehaviour runesPrefab;
    public ComboManager comboManager;
    public int playMode = 0;
    public int stackSize = 10;
    //public float scalefactor;


    private void Awake()
    {
        if ((object)i != null)
            Destroy(i);
        i = this;
        //scalefactor = Screen.width*0.208f/100f;
        //scalefactor = 1;
        stackManager.startStack();
    }
    void Start()
    {
        ReplenishRunes(0, stackSize);
        Init();
    }

    public void ClearRunes()
    {
        Transform runesContainer = stackManager.runeBehaviour.transform;
        for (int i = 0; i < runesContainer.childCount; i++)
        {
            Destroy(runesContainer.GetChild(i).gameObject);
        }
    }
    public void ArrangeRunes()
    {
        float height = runesPrefab.GetHeight();
        Transform runesContainer = stackManager.runeBehaviour.transform;
        for (int i = 0; i < runesContainer.childCount; i++)
        {
            runesContainer.GetChild(i).transform.localPosition = new Vector3(0, -height / 2 - height * i + stackManager.runeBehaviour.GetHeight() / 2, 0);
        }
    }
    public void ReplenishRunes(int start, int finish)
    {
        //Debug.Log("Replenish... " + start + ", " + finish);
        float height = runesPrefab.GetHeight();

        //float localScale = scalefactor;
        //Debug.Log("scale factor: " + scalefactor);
        for (int i = start; i < finish; i++)
        {
            GameObject rune = Instantiate(runesPrefab.gameObject);
            rune.transform.SetParent(stackManager.runeBehaviour.transform);
            rune.transform.localPosition = new Vector3(0, -height / 2f - height * i + stackManager.runeBehaviour.GetHeight() / 2f, 0);
            rune.transform.localScale = Vector3.one;
            //rune.transform.localScale = new Vector3(localScale, localScale, localScale);
            //rune.GetComponent<RectTransform>().rect.Set(0,-height / 2 - height * i + stackManager.runeBehaviour.GetHeight() / 2f, width, height);
            //log.text += " " + i + ".-position: " + rune.transform.position;
            ElementBehaviour runeBehaviour = rune.GetComponent<RuneUIBehaviour>();
            runeBehaviour.setElement(new Rune((byte)Random.Range(1, 256)));
            runeBehaviour.index = (byte)i;
        }
    }

    public int GetMagicIndex(int magic)
    {
        switch (magic)
        {
            case 1:
                return 0;
            case 2:
                return 1;
            case 4:
                return 2;
            case 8:
                return 3;
            case 16:
                return 4;
            case 32:
                return 5;
            case 64:
                return 6;
            case 128:
                return 7;
        }
        return -1;
    }

    public abstract void PutRune(ElementBehaviour element);

    public abstract void RemoveRune(ElementBehaviour element);

    public abstract void PutSpell(ElementBehaviour element);

    public abstract void RemoveSpell(ElementBehaviour element);

    public abstract int GetCurrentPlayerType();

    public abstract Player GetEnemyPlayer();

    public abstract void Init();

    public abstract void ExitMatch();

    public abstract void setTurnAction();
}
