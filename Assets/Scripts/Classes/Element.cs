﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Element {
    public abstract byte GetCriptoGrama();
    public abstract void SetCriptoGram(byte criptogram);
}
