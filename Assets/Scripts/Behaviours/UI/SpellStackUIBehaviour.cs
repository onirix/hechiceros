﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellStackUIBehaviour : StackBehaviour
{
    float scalefactor;
    Rect rect;
    // Use this for initialization
    void Awake()
    {
        scalefactor = Screen.width / 480f;
        rect = GetComponent<RectTransform>().rect;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void updateStack(ElementBehaviour spellBehaviour)
    {
        List<ElementBehaviour> spellsInStack = GameManager.i.stackManager.spellsInStack;
        float width = spellBehaviour.GetWidth();
        byte spell = spellBehaviour.element.GetCriptoGrama();
        if (spellBehaviour.inStack)
        {
            spellBehaviour.Destroy();
            bool delete = true;
            for (int i = 0; i < spellsInStack.Count; i++)
            {
                spellsInStack[i].transform.localPosition = new Vector3(width / 2 - width * (i + 1) + GetWidth() / 2 - 10f, 0, 0);
                int sp = (spellsInStack[i].element.GetCriptoGrama() ^ spell);
                if (delete && sp == 0)
                {
                    delete = false;
                }
            }
            if (delete)
            {
                GameManager.i.stackManager.runeResult.spellResult &= (byte)~spell;
                GameManager.i.stackManager.runeResult.DisplaySpell();
            }
        }
        else
        {
            //float scaler = GameManager.i.scalefactor;
            //spellBehaviour.transform.localScale = new Vector3(scaler,scaler,scaler);
            spellBehaviour.transform.SetParent(transform);
            spellBehaviour.transform.localScale = Vector3.one;
            spellBehaviour.transform.localPosition = new Vector3(width / 2 - width * spellsInStack.Count + GetWidth() / 2 - 10f, 0, 0);
            spellBehaviour.inStack = true;
            if ((GameManager.i.stackManager.runeResult.spellResult & spell) == 0)
            {
                GameManager.i.stackManager.runeResult.spellResult |= spell;
                GameManager.i.stackManager.runeResult.DisplaySpell();
            }
        }
    }
    public override float GetWidth()
    {
        return rect.width;
    }
    public override float GetHeight()
    {
        return rect.height;
    }
    public override void ClearStack()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public override void RefreshStack()
    {
        throw new System.NotImplementedException();
    }
}
