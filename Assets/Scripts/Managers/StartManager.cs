﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartManager : MonoBehaviour {
    DataManager dataManager;
    SceneChanger sceneChanger;

	// Use this for initialization
	void Awake () {
        GetComponent<DataManager>().Load();
        sceneChanger = GetComponent<SceneChanger>();
    }

    public void History(){
        PlayerPrefs.SetInt("Next", 1);
        PlayerPrefs.SetString("NextScene", "History");

        if (PlayerPrefs.GetInt("SelectedRace",-1) == -1)
        {
            sceneChanger.ChangeScene("Characters");
        }
        else
        {
            sceneChanger.ChangeScene("History");
        }
    }
    public void Battle()
    {
        PlayerPrefs.SetString("NextScene", "Battle");
        PlayerPrefs.SetInt("Next", 0);
        sceneChanger.ChangeScene("Characters");
    }
    public void Training()
    {
        PlayerPrefs.SetString("NextScene", "Training");
        sceneChanger.ChangeScene("Training");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
