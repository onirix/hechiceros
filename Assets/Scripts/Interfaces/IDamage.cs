﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamage  {

    int TakeDamage(Damage damage);
    Damage MakeDamage(Damage damage);
    byte GetMagicAfinity();
}
