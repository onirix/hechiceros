﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorData", menuName = "Objects/ColorData")]
public class ColorsData : ScriptableObject {

    [SerializeField]
    Color[] colors;

    public Color GetColor(int i)
    {
        return colors[i];
    }
}
