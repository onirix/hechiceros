﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialBehaviour : MonoBehaviour
{

    public bool inProgress = false;

    [SerializeField]
    LessonBehaviour[] lessons;

    int indexLesson = 0;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DisplayTutorial()
    {
        inProgress = true;
        if (indexLesson > 0)
        {
            EnableLesson(indexLesson - 1, false);
        }
        EnableLesson(indexLesson, true);
    }
    public void NextLesson()
    {
        EnableLesson(indexLesson, false);
        indexLesson++;
        EnableLesson(indexLesson, true);
    }
    public void EnableLesson(int index, bool enable)
    {

        lessons[indexLesson].gameObject.SetActive(enable);
        lessons[index].EnableObjectLesson(!enable);
        lessons[index].EnableAnimtionLesson(enable);
    }
    public void StartPractice()
    {
        lessons[indexLesson].EnableAnimtionLesson(false);
        lessons[indexLesson].gameObject.SetActive(false);
        indexLesson++;
    }
    public void FinishTutorial()
    {
        inProgress = false;
        EnableLesson(lessons.Length - 1, false);
    }
}
