﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    [SerializeField]
    DialogBehaviour dialog;

    public string previousscene;

    private void Awake()
    {
        if ((object)dialog != null)
        {
            dialog.positive += Previous;
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            if (previousscene != "")
            {
                if ((object)dialog == null)
                    //Debug.Log("Loading previous scene: " + previousscene);
                    Previous();
                else
                    dialog.DisplayDialog();
            }
        }

    }
    public void Next()
    {
        string next = PlayerPrefs.GetString("NextScene", "");
        if (next.Length > 0)
        {
            SceneManager.LoadScene(next);
        }
    }

    public void Previous()
    {
        SceneManager.LoadScene(previousscene);
    }

    public void ChangeScene(string changeto)
    {
        SceneManager.LoadScene(changeto);
    }
}
