﻿
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData {
    public float experience;
    public int level;
    public int history;
    public int wins;
    public int loses;
    public int selectedRace = -1;
    public int[,] race;
}
