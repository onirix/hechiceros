﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class RuneUIBehaviour : ElementBehaviour, IPointerDownHandler {
	[SerializeField]
	Text criptagramText;

    [SerializeField]
    GameObject runeImagePrefab;

    int lasCount = 1;

	// Use this for initialization
	void Awake () {
    }

	// Update is called once per frame
	void Update () {
        /*if (lasCount != transform.childCount && transform.childCount > 1){
            float offset = GetWidth()/2f;

            for (int i = 1; i < transform.childCount; i++)
            {
                RectTransform childTransform = (RectTransform) transform.GetChild(i);
                if (i == 1)
                    offset = childTransform.rect.width * (transform.childCount - 1f) / 2f;
                childTransform.localPosition = new Vector3(childTransform.rect.width * (i - 1f) - offset, 0,0);
            }
        }*/
	}
    public override void setElement(Element element){
        this.element = element;
        displayElement();
	}
	public override void putnStack (){
        if (GameManager.i.GetCurrentPlayerType() == 0)
        {
            if (inStack)
            {
                GameManager.i.RemoveRune(this);
            }
            else
            {
                GameManager.i.PutRune(this);
            }
            inStack = !inStack;
        }
	}

    public override Element process()
	{
        GameManager.i.stackManager.runesInStack.Remove(this);
        Destroy(gameObject);
		return element;
	}
    public override float GetHeight()
    {
        return GetComponent<RectTransform>().rect.height;
    }
    public override float GetWidth()
    {
        return GetComponent<RectTransform>().rect.width;
    }

    public override void displayElement()
    {
        byte cripto = element.GetCriptoGrama();
        criptagramText.text = "" + cripto;
        int i = 0;
        List<RectTransform> runes = new List<RectTransform>();
        float offset = GetWidth() / 2f;
        //float scalefactor = GameManager.i.scalefactor; ;
        while (cripto != 0){
            if ((cripto&1)!=0){
                GameObject rune = Instantiate(runeImagePrefab);
                //rune.transform.localScale = new Vector3(scalefactor,scalefactor,scalefactor);
                RuneDisplayBehaviour runeImage = rune.GetComponent<RuneDisplayBehaviour>();
                runeImage.SetRune(transform, i);
                rune.transform.localScale = Vector3.one;
                RectTransform runeRectTransform = (RectTransform) rune.transform;
                rune.transform.localPosition = new Vector3((runeRectTransform.rect.width + 1) * i - offset + runeRectTransform.rect.width/2f, 0f, 0);
            }
            cripto = (byte) (cripto >> 1);
            i++;
        }
    }

    public override void Destroy()
    {
        throw new NotImplementedException();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        GameManager.i.soundManager.ClicSound(); 
        putnStack();
    }
}
