﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StackBehaviour : MonoBehaviour {

    public abstract void RefreshStack();

	public abstract void updateStack(ElementBehaviour runeBehaviour);

    public abstract float GetHeight();
    public abstract float GetWidth();
    public abstract void ClearStack();
}
