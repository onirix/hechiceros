﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryManager : GameManager
{
    [SerializeField]
    Race[] historyCharacters;

    [SerializeField]
    TimerBehaviour timer;

    /*[SerializeField]
    Text reward;*/

    public Player[] players;

    int playersTurn = 0;

    float exp;

    public override void Init()
    {
        PlayerPrefs.SetInt("GameType", 1);
        playMode = PlayerPrefs.GetInt("PlayMode");

        for (int i = 0; i < players.Length; i++)
        {
            int index = 0;
            if (i == 0)
            {
                index = PlayerPrefs.GetInt("Race");
                if (index == 0)
                {
                    index = Random.Range(1, 9);
                }

                int level = PlayerPrefs.GetInt("Level");
                players[i].InstantitePlayer(i + 1, races[index - 1], level);
            }
            else
            {
                int historyIndex = PlayerPrefs.GetInt("History");

                //Debug.Log("History Index: " + historyIndex);
                if (historyIndex < 24)
                {
                    players[i].InstantitePlayer(i + 1, historyCharacters[historyIndex], historyCharacters[historyIndex].level);
                }
                else
                {
                    int level = historyIndex / 24;
                    historyIndex %= 24;
                    //Debug.Log("history index: " + (historyIndex) + " lenght: " + historyCharacters.Length);
                    players[i].InstantitePlayer(i + 1, historyCharacters[historyIndex], historyCharacters[historyIndex].level + level);
                }

                exp = 10 * players[i].race.level + historyIndex * 5;
                //reward.text = "" + exp;
                int magicIndex = GetMagicIndex(players[i].race.magic);
                bgManager.SetBackground(magicIndex);
                musicManager.SetMusic(magicIndex);
            }
            players[i].life = 100;
        }
        players[playersTurn].SetTurn();
    }

    private int next()
    {
        return (playersTurn + 1) % 2;
    }
    public override Player GetEnemyPlayer()
    {
        return players[next()];
    }
    public override int GetCurrentPlayerType()
    {
        return players[playersTurn].type;
    }

    public override void setTurnAction()
    {
        stackManager.runeResult.element.SetCriptoGram(0);
        Damage damage = players[playersTurn].MakeDamage(stackManager.resolveSpell());
        Debug.Log("Damage amoung: " + damage.amoung);
        int magic = damage.magic;
        int strongest = players[playersTurn].race.strongest;
        playersTurn = next();
        int weakness = players[playersTurn].race.weakness;
        int afinity = players[playersTurn].race.afinity;
        comboManager.CheckCombo(magic, strongest, weakness, afinity, damage.amoung);
        int life = players[playersTurn].TakeDamage(damage);
        players[playersTurn].playerManager.setLife(life);
        //life = 0;
        if (life > 0)
        {
            players[playersTurn].SetTurn();
        }
        else
        {
            players[playersTurn].Lose();
            players[next()].Win(exp);
        }
        timer.ResetTimer();

        if (playMode == 0)
        {
            ClearRunes();
            ReplenishRunes(0, stackSize);
        }
        else
        {
            int start = stackManager.runeBehaviour.transform.childCount;
            int finish = stackSize - start;
            ArrangeRunes();
            ReplenishRunes(start, finish);
        }
    }

    public override void PutRune(ElementBehaviour element)
    {
        stackManager.putRune(element);
    }

    public override void RemoveRune(ElementBehaviour element)
    {
        stackManager.removeRune(element);
    }

    public override void PutSpell(ElementBehaviour element)
    {
        stackManager.putSpell(element);
    }

    public override void RemoveSpell(ElementBehaviour element)
    {
        stackManager.removeSpell(element);
    }
    public override void ExitMatch()
    {
        players[0].Lose();
    }
}
