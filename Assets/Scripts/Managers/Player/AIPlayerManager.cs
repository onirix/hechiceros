﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayerManager : PlayerManager
{

    float currentTime = 0;

    WaitBehaviour wait;

    private void Start()
    {
        wait = GameObject.FindWithTag("wait").GetComponent<WaitBehaviour>();
        currentTime = Time.time;
    }

    private void Update()
    {
        if (haveTurn)
        {
            if (Time.time - currentTime > 4)
            {
                ResolveTurn();
            }
        }
    }

    public override void ResolveTurn()
    {
        wait.Hide();
        haveTurn = false;
        GameManager.i.setTurnAction();
    }

    public override void setLife(int life)
    {
        playerLife.SetLife(life);
    }

    public override void setTurn()
    {
        wait.Show();
        currentTime = Time.time;
        haveTurn = true;
    }
}

