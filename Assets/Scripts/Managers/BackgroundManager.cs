﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundManager : MonoBehaviour {

    Image image;

    [SerializeField]
    BackgroundData backgrounds;
    [SerializeField]
    WindowsData windowsData;
    [SerializeField]
    ColorsData colorsData;


    public void Awake()
    {
        image = GetComponent<Image>();
    }

    public void SetBackground(int magic)
    {
        //Debug.Log("Setting background: " + magic);
        if (magic == -1)
        {
            magic = Random.Range(0, 8);
        }
        image.sprite = backgrounds.GetBackGround(magic);
        image.color = colorsData.GetColor(magic);
        SettingWindows(magic);
    }
    private void SettingWindows(int i)
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("window");

        foreach (GameObject _gameObject in gameObjects)
        {
            _gameObject.GetComponent<Image>().sprite = windowsData.GetWindow(i);
        }
    }
}
