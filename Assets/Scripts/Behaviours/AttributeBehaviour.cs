﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeBehaviour : MonoBehaviour {

    [SerializeField]
    RuneData runeData;


    public void SetImageAttribute(int index)
    {
        GetComponent<Image>().sprite = runeData.GetSprite(index);
    }
}
