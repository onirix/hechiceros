﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ElementBehaviour : MonoBehaviour {
    [SerializeField]
    public Element element;
    public byte index;
    public bool inStack = false;

    public abstract void setElement(Element element);
    public abstract void displayElement();
	public abstract void putnStack();
	public abstract Element process();
    public abstract float GetHeight();
    public abstract float GetWidth();
    public abstract void Destroy();
}
