﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rune: Element {

    public byte criptogram;

    public Rune(byte criptagram){
		this.criptogram = criptagram;
	}

    public override byte GetCriptoGrama()
    {
        return criptogram;;
    }

    public override void SetCriptoGram(byte criptogram)
    {
        this.criptogram = criptogram;;
    }
}
