﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaticsBehaviour : MonoBehaviour
{
    [SerializeField]
    Text score;

    [SerializeField]
    Text wins;

    [SerializeField]
    Text loses;

    [SerializeField]
    Text level;

    // Use this for initialization
    void Start()
    {


        //PlayerPrefs.SetInt("Exp", 0);
        //PlayerPrefs.SetInt("History", 0);
        //PlayerPrefs.SetInt("Level", 0);
        score.text = "Score: " + PlayerPrefs.GetFloat("Exp", 0);
        level.text = "Level: " + PlayerPrefs.GetInt("Level", 1);
        wins.text = "Wins: " + PlayerPrefs.GetInt("Wins", 0);
        loses.text = "Loses: " + PlayerPrefs.GetInt("Loses", 0);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
