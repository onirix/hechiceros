﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExperienceManager : MonoBehaviour
{
    [SerializeField]
    SoundManager soundManager;
    [SerializeField]
    Slider sliderExp;
    [SerializeField]
    Text levelText;
    [SerializeField]
    Text expText;
    [SerializeField]
    Text winsText;
    [SerializeField]
    Text losesText;

    /*[SerializeField]
    Animator levelUpAnim;*/
    float XP;
    int xpaccum = 0;
    int currentlevel;

    // Use this for initialization
    void Awake()
    {
        currentlevel = PlayerPrefs.GetInt("Level");
        float xp = PlayerPrefs.GetFloat("GainedExp");
        //Debug.Log("XP: " + PlayerPrefs.GetFloat("Exp"));
        if (xp > 0)
        {
            //Debug.Log("xp: " + xp);
            XPGained(xp);
        }
        else
        {
            XP = PlayerPrefs.GetFloat("Exp");
            //Debug.Log("XP: " + XP);
            ProgressXPCal(XP);
        }

        levelText.text = "Level: " + currentlevel;
        expText.text = "Exp: " + XP;
        winsText.text = "Wins: " + PlayerPrefs.GetInt("Wins");
        losesText.text = "Loses: " + PlayerPrefs.GetInt("Loses");
        GameManager.i.dataManager.Save();
    }
    private void Start()
    {
        if (PlayerPrefs.GetInt("isWin") == 1)
        {
            soundManager.WinSound();
        }
        else
        {
            soundManager.LoseSound();
        }
    }
    private void Update()
    {
        if ((int)sliderExp.value < xpaccum)
        {
            sliderExp.value += 1;
        }
    }

    public void XPGained(float xp)
    {
        XP = PlayerPrefs.GetFloat("Exp") + xp;
        currentlevel = (int)(0.1f * Mathf.Sqrt(XP));
        //Debug.Log("curvl: " + curvl);
        PlayerPrefs.SetInt("Level", currentlevel);
        //levelUpAnim.SetTrigger("levelup");
        PlayerPrefs.SetFloat("Exp", XP);
        ProgressXPCal(XP);
    }
    public void ProgressXPCal(float XP)
    {
        float xpnextlevel = 100 * (currentlevel + 1) * (currentlevel + 1);
        float diffrencexp = xpnextlevel - XP;
        float totaldiference = xpnextlevel - 100 * currentlevel * currentlevel;
        Debug.Log("Next Level XP: " + xpnextlevel);
        Debug.Log("Diff Level XP: " + diffrencexp);
        Debug.Log("Tolt Level XP: " + totaldiference);
        xpaccum = (int)((totaldiference - diffrencexp) * 100 / totaldiference);
    }
}
