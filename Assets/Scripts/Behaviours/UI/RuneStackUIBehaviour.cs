﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneStackUIBehaviour : StackBehaviour {


	Rect rect;
	// Use this for initialization
	void Awake () {
        rect = GetComponent<RectTransform>().rect;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void updateStack(ElementBehaviour runeBehaviour)
	{
        float height = runeBehaviour.GetHeight();
		runeBehaviour.transform.SetParent(transform);
        runeBehaviour.transform.localPosition = new Vector3(0, -height / 2 - height * GameManager.i.stackManager.runesInStack.Count + GetHeight()/2, 0);
        runeBehaviour.transform.localScale = Vector3.one;
        //GameManager.i.stackManager.runeResult.AddMagicElement(runeBehaviour.element);
    }
    public override float GetWidth()
    {
        return rect.width;
    }
    public override float GetHeight()
    {
        return rect.height;
    }

    public override void ClearStack()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public override void RefreshStack()
    {

        float height = 0;
        for (int i=0; i < GameManager.i.stackManager.runesInStack.Count; i++)
        {
            ElementBehaviour runeBehaviour = GameManager.i.stackManager.runesInStack[i];
            if (i==0)
                height = runeBehaviour.GetHeight();
            runeBehaviour.transform.localPosition = new Vector3(0, -height / 2 - height * i + GetHeight() / 2, 0);
        }
    }
}
