﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class DataManager : MonoBehaviour
{

    public void Save()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream fileStream = File.Open(Application.persistentDataPath + "/plyr.dat", FileMode.OpenOrCreate);
        PlayerData playerData = new PlayerData();
        playerData.experience = PlayerPrefs.GetFloat("Exp");
        playerData.level = PlayerPrefs.GetInt("Level");
        playerData.history = PlayerPrefs.GetInt("History");
        playerData.wins = PlayerPrefs.GetInt("Wins");
        playerData.loses = PlayerPrefs.GetInt("Loses");
        binaryFormatter.Serialize(fileStream, playerData);
        fileStream.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/plyr.dat"))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = File.Open(Application.persistentDataPath + "/plyr.dat", FileMode.Open);
            PlayerData playerData = (PlayerData)binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            PlayerPrefs.SetFloat("Exp", playerData.experience);
            PlayerPrefs.SetInt("Level", playerData.level);
            PlayerPrefs.SetInt("History", playerData.history);
            PlayerPrefs.SetInt("Wins", playerData.wins);
            PlayerPrefs.SetInt("Loses", playerData.loses);
        }
    }
}
