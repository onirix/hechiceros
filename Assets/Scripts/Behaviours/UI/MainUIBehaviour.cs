﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUIBehaviour : StackBehaviour {


    Rect rect;
    // Use this for initialization
    void Awake()
    {
        rect = GetComponent<RectTransform>().rect;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void updateStack(ElementBehaviour runeBehaviour)
    {
        Debug.Log("Updating main stack: " + name);
        float height = runeBehaviour.GetHeight();
        runeBehaviour.transform.SetParent(transform);
        runeBehaviour.transform.localPosition = new Vector3(0, -height / 2 - height * runeBehaviour.index + GetHeight() / 2, 0);

        //runeBehaviour.transform.position = new Vector3(GetWidth() / 2, -runeBehaviour.GetHeight() / 2 - runeBehaviour.GetHeight() * runeBehaviour.index + GetHeight(), 0);

    }
    public override float GetWidth()
    {
        return rect.width;
    }
    public override float GetHeight()
    {
        return rect.height;
    }

    public override void ClearStack()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public override void RefreshStack()
    {
        throw new System.NotImplementedException();
    }
}
