﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundUIData", menuName = "Objects/Sounds/SoundUIData")]
public class SoundUIData : SoundData
{

    public AudioClip ClicSound()
    {
        return audios[0];
    }

    public AudioClip AcceptSound()
    {
        return audios[1];
    }

    public AudioClip ExitSound()
    {
        return audios[2];

    }

    public AudioClip WinSound()
    {
        return audios[3];
    }
    public AudioClip LoseSound()
    {
        return audios[4];
    }
}
