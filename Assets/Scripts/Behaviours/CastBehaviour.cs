﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CastBehaviour : MonoBehaviour, IPointerUpHandler {
    public delegate void Resolve();
    public Resolve resolve;

    Button button;
	// Use this for initialization
	void Awake () {
        button = GetComponent<Button>();
	}



    public void SetEnable(bool enable){
        button.enabled = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (resolve != null){
            resolve();
        }else{
            Debug.Log("Not resolve finded");
        }
    }
}
