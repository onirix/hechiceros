﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WindowsData", menuName = "Objects/WindowsData")]
public class WindowsData : ScriptableObject
{
    public Sprite[] sprites;

    public Sprite GetWindow(int index)
    {
        return sprites[index];
    }
}
