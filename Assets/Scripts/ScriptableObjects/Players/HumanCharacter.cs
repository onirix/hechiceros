﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HumanPlayer", menuName = "Player/Human")]
public class HumanCharacter : Player
{

    public override byte GetMagicAfinity()
    {
        return race.GetMagicAfinity();
    }

    public override Damage MakeDamage(Damage damage)
    {
        if (damage.magic == 0)
            return damage;
        damage.amoung = level * DAMAGE;
        damage.amoung *= damage.amoung;
        return race.MakeDamage(damage);
    }

    public override void SetTurn()
    {
        playerManager.setTurn();
    }

    public override int TakeDamage(Damage damage)
    {
        int amoung = race.TakeDamage(damage);
        if (amoung > 0)
        {
           GameManager.i.soundManager.DamageSound(race.genere);
        }
        life -= amoung;
        return life;
    }
    public override void Win(float exp)
    {
        Debug.Log("exp: " + exp);
        GameManager.i.soundManager.WinSound();
        PlayerPrefs.SetInt("isWin",1);
        Debug.Log("PLayer 1 win");
        int game = PlayerPrefs.GetInt("GameType");
        if (game == 1)
        {
            int history = PlayerPrefs.GetInt("History") + 1;
            PlayerPrefs.SetInt("History", history);
        }
        int wins = PlayerPrefs.GetInt("Wins") + 1;
        PlayerPrefs.SetInt("Wins", wins);
        PlayerPrefs.SetFloat("GainedExp", exp);
        GameManager.i.sceneChanger.ChangeScene("ScoreBoard");
    }

    public override void Lose()
    {

        PlayerPrefs.SetInt("isWin", 0);
        int loses = PlayerPrefs.GetInt("Loses") + 1;
        PlayerPrefs.SetInt("Loses", loses);
        GameManager.i.dataManager.Save();
        GameManager.i.sceneChanger.ChangeScene("ScoreBoard");
    }
}
