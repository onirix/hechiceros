﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitBehaviour : MonoBehaviour {
    [SerializeField]
    Image portrait;
    [SerializeField]
    RectTransform strongestContainer;
    [SerializeField]
    RectTransform weaknessContainer;
    [SerializeField]
    RectTransform afinityContainer;
    [SerializeField]
    Text textName;
    [SerializeField]
    Text textLevel;

    [SerializeField]
    GameObject imgAttrPrefab;

    public void DisplayPlayer(Race race, int level){
        portrait.sprite = race.sprite;
        byte strongest = race.strongest;
        byte weakness = race.weakness;
        byte afinity = race.afinity;
        ProcessAttribute(strongest, strongestContainer);
        ProcessAttribute(weakness, weaknessContainer);
        ProcessAttribute(afinity, afinityContainer);
        textName.text = race.name;
        textLevel.text = "" + level;
    }
    private int GetIndex(byte magic){
        int index = -1;
        while(magic != 0){
            magic = (byte)(magic >> 1);
            index++;
        }
        return index;
    }
    private void ProcessAttribute(byte attribute, RectTransform container){
        int i = 0;
        int count = 0;
        while (attribute != 0)
        {
            if ((attribute & 1) == 1)
            {
                CreateAttribute(i,count, container);
                count++;
            }
            attribute = (byte)(attribute >> 1);
            i++;
        }
    }
    private void CreateAttribute(int i, int count, RectTransform container){
        GameObject strgo = Instantiate(imgAttrPrefab);
        RectTransform strtrec = strgo.GetComponent<RectTransform>();
        strtrec.SetParent(container);
        strtrec.localPosition = new Vector3(count*strtrec.rect.width, 0, 0);
        strtrec.localScale = Vector3.one;
        AttributeBehaviour image = strgo.GetComponent<AttributeBehaviour>();
        //image.color = GetMagicColor(i);
        image.SetImageAttribute(i);
    }

    private Color GetMagicColor(int i){
        //Debug.Log("color: " + i);
        switch (i)
        {
            case 0:
                return Color.white;
            case 1:
                return Color.yellow;
            case 2:
                return Color.blue;
            case 3:
                return Color.green;
            case 4:
                return Color.red;
            case 5:
                return Color.grey;
            case 6:
                return Color.magenta;
            case 7:
                return Color.black;
        }
        return Color.cyan;
    }
}
