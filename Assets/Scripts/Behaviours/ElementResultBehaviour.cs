﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ElementResultBehaviour : ElementBehaviour
{

    public byte spellResult;
    public abstract void DisplaySpell();
    public abstract void DisplayMagic();
    public abstract void AddMagicElement(Element element);
    public abstract void SetMagicElement(Element element);

}
