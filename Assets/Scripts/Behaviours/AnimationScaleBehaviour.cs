﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationScaleBehaviour : MonoBehaviour
{

    [SerializeField]
    float threshold;

    [SerializeField]
    float offsetTime;

    float startTime;

    //Vector3 scaleTo;

    bool startAnimation = false;
    //bool swap = false;
    // Use this for initialization

    Outline outline;

    float markTime;

    private void Awake()
    {
        outline = GetComponent<Outline>();
    }

    void Start()
    {
        //startTime = Time.time;
        //scaleTo = transform.localScale * 1.2f;
    }

    // Update is called once per frame
    void Update()
    {
        if (startAnimation)
        {
            if (Time.time - startTime > offsetTime)
            {
                if (Time.time - markTime > threshold)
                {
                    outline.enabled = !outline.enabled;
                    markTime = Time.time;
                }
                /*if (Vector3.Distance(scaleTo, transform.localScale) > 0.01f)
                {
                    transform.localScale = Vector3.Lerp(transform.localScale, scaleTo, Time.deltaTime * speed);
                }
                else
                {
                    if (swap)
                        scaleTo = transform.localScale * 1.2f;
                    else
                        scaleTo = Vector3.one;
                    swap = !swap;
                }
                startTime = Time.time;*/

            }
        }
    }
    public void EnableAnimationScale(bool enable)
    {
        startAnimation = enable;
        outline.enabled = enable;
        startTime = Time.time;
        markTime = startTime;
    }
}
