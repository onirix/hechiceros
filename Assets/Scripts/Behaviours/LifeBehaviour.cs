﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBehaviour : MonoBehaviour {
    Slider life;

	// Use this for initialization
	void Start () {
        life = GetComponent<Slider>();
	}

    public void SetLife(int life)
    {
        this.life.value = life;
    }
}
