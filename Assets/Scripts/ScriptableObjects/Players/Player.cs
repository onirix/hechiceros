﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Player : ScriptableObject, IDamage {

    [SerializeField]
    GameObject playerPrefab;

    public PlayerManager playerManager;
    public const int DAMAGE = 2;
    public int level;
    public int life;
    public Race race;
    public int type;

    public abstract Damage MakeDamage(Damage damage);
    public abstract int TakeDamage(Damage damage);
    public abstract void SetTurn();
    public abstract byte GetMagicAfinity();
    public abstract void Win(float exp);
    public abstract void Lose();

    public void InstantitePlayer (int playerNumber, Race race, int level){
        this.race = race;
        playerManager = Instantiate(playerPrefab).GetComponent<PlayerManager>();
        playerManager.Display(race, playerNumber, level);
    }

}
