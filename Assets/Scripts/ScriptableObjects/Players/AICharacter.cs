﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ComputerPlayer", menuName = "Player/Computer")]
public class AICharacter : Player
{
    [SerializeField]
    int critical_threshold;
    [SerializeField]
    int mistake_threshold;
    [SerializeField]
    int lucky_threshold;
    [SerializeField]
    int concentration_threshold;
    [SerializeField]
    int delayTime;

    int fury;


    public override byte GetMagicAfinity()
    {
        return race.GetMagicAfinity();
    }

    public override void Lose()
    {
    }

    public override Damage MakeDamage(Damage damage)
    {
        Debug.Log("level: " + level + " damage: " + DAMAGE);
        damage.amoung = level * DAMAGE;
        damage.magic = race.magic;
        int magic = Random.Range(0, 8);
        byte[] magics = new byte[] { 1, 2, 4, 8, 16, 32, 64, 128 };
        damage.magic |= magics[magic];
        int attack_will = Random.Range(fury + race.level, 9);
        int concentration = Random.Range(fury + race.level, 9);
        int weak_lucky = Random.Range(fury + race.level, 9);
        int error_mistake = Random.Range(0, 9 - race.level - fury);
        //Debug.Log("magic created: " + damage.magic);
        Debug.Log("attack_will: " + attack_will + " weak_lucky: " + weak_lucky + " error_mistake: " + error_mistake + " concetration: " + concentration);
        Player enemy = GameManager.i.GetEnemyPlayer();
        if (attack_will >= critical_threshold)
        {
            Debug.Log("Have attack will");
            damage.magic |= race.strongest;
            fury = 0;
        }
        if (weak_lucky >= lucky_threshold)
        {
            Debug.Log("Have lucky");
            damage.magic |= enemy.race.weakness;
            fury = 0;
        }
        if (weak_lucky < lucky_threshold && attack_will < critical_threshold)
        {
            Debug.Log("I get Angry");
            fury++;
        }
        if (concentration >= concentration_threshold)
        {
            Debug.Log("Have concentration");
            damage.magic &= (byte) (~enemy.race.strongest);
        }
        if (error_mistake > mistake_threshold)
        {
            Debug.Log("Mistake");
            damage.magic |= enemy.race.afinity;
            fury++;
        }
        else
        {
            damage.magic &= (byte)(~enemy.race.afinity);
        }

        if (fury>8){
            fury = 8;
        }
        Debug.Log("final magic: " + damage.magic + " fury: " + fury + " damage: " + damage.amoung);
        return race.MakeDamage(damage);
    }

    public override void SetTurn()
    {
        playerManager.setTurn();
    }

    public override int TakeDamage(Damage damage)
    {
        int amoung = race.TakeDamage(damage);
        life -= amoung;
        Debug.Log("computer damage: " + amoung);
        return life;
    }

    public override void Win(float exp)
    {

    }
}
