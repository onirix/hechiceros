﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {
    [SerializeField]
    protected SoundUIData soundData;

    protected AudioSource audioSource;
	// Use this for initialization
	void Awake () {
        audioSource = GetComponent<AudioSource>();
	}

    public void ClicSound()
    {
        audioSource.clip = soundData.ClicSound();
        audioSource.Play();
    }

    public void AcceptSound()
    {
        audioSource.clip = soundData.AcceptSound();
        audioSource.Play();
    }

    public void ExitSound()
    {
        audioSource.clip = soundData.ExitSound();
        audioSource.Play();

    }
    public void WinSound()
    {
        audioSource.clip = soundData.WinSound();
        audioSource.Play();
    }

    public void LoseSound()
    {
        audioSource.clip = soundData.LoseSound();
        audioSource.Play();

    }
}
