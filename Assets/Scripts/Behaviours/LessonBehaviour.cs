﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LessonBehaviour : MonoBehaviour {
    [SerializeField]
    AnimationScaleBehaviour[] animations;
    [SerializeField]
    GameObject[] gameObjects;

    public void EnableAnimtionLesson(bool enable){
        foreach (AnimationScaleBehaviour animation in animations){
            animation.EnableAnimationScale(enable);
        }
    }
    public void EnableObjectLesson(bool enable)
    {
        foreach (GameObject gameObject in gameObjects)
        {
            gameObject.SetActive(enable);
        }

    }
}
