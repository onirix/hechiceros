﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundData", menuName = "Objects/Sounds/SoundData")]
public class SoundData : ScriptableObject
{
    public AudioClip[] audios;

}
