﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RuneData", menuName = "Objects/RuneData")]
public class RuneData : ScriptableObject {

    public Sprite[] sprites;


    public Sprite GetSprite(int magic)
    {
        return sprites[magic];
    }
}
