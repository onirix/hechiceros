﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapBehaviour : MonoBehaviour {
    [SerializeField]
    Transform snapTo;
	// Use this for initialization
	void Start () {
        transform.position = snapTo.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
