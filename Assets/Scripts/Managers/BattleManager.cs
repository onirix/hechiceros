﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : GameManager
{

    [SerializeField]
    Race[] historyCharacters;
    [SerializeField]
    TimerBehaviour timer;
    [SerializeField]

    public Player[] players;

    int playersTurn = 0;
    float exp = 0;

    public override void Init()
    {
        PlayerPrefs.SetInt("GameType", 2);
        playMode = PlayerPrefs.GetInt("PlayMode");
        for (int i = 0; i < players.Length; i++)
        {
            int index = 0;
            int level = 0;
            if (i == 0)
            {
                index = PlayerPrefs.GetInt("Race");
                if (index == 0)
                {
                    index = Random.Range(1, 9);
                }
                level = PlayerPrefs.GetInt("Level");
                players[i].InstantitePlayer(i + 1, races[index - 1], level);
            }
            else
            {
                index = PlayerPrefs.GetInt("Enemy");
                if (index == 0)
                    index = Random.Range(1, 25);

                level = historyCharacters[index - 1].level;
                players[i].InstantitePlayer(i + 1, historyCharacters[index - 1], level);
                int magicIndex = GetMagicIndex(players[i].race.magic);
                bgManager.SetBackground(magicIndex);
                musicManager.SetMusic(magicIndex);
                exp = 20 * players[i].race.level;
            }
            players[i].life = 100;
        }
        Debug.Log("main runes tranform: " + stackManager.runeBehaviour.transform);
        players[playersTurn].SetTurn();
    }

    private int next()
    {
        return (playersTurn + 1) % 2;
    }
    public override Player GetEnemyPlayer()
    {
        return players[next()];
    }
    public override int GetCurrentPlayerType()
    {
        return players[playersTurn].type;
    }

    public override void setTurnAction()
    {
        stackManager.runeResult.element.SetCriptoGram(0);
        Damage damage = players[playersTurn].MakeDamage(stackManager.resolveSpell());
        int magic = damage.magic;
        int strongest = players[playersTurn].race.strongest;
        playersTurn = next();
        int weakness = players[playersTurn].race.weakness;
        int afinity = players[playersTurn].race.afinity;
        comboManager.CheckCombo(magic, strongest, weakness, afinity, damage.amoung);

        int life = players[playersTurn].TakeDamage(damage);
        players[playersTurn].playerManager.setLife(life);
        if (life > 0)
        {
            players[playersTurn].SetTurn();
        }
        else
        {
            players[playersTurn].Lose();
            players[next()].Win(exp);
        }
        timer.ResetTimer();

        if (playMode == 0)
        {
            ClearRunes();
            ReplenishRunes(0, stackSize);
        }
        else
        {
            int start = stackManager.runeBehaviour.transform.childCount;
            int finish = stackSize - start;
            ArrangeRunes();
            ReplenishRunes(start, finish);
        }
    }

    public override void PutRune(ElementBehaviour element)
    {
        stackManager.putRune(element);
    }

    public override void RemoveRune(ElementBehaviour element)
    {
        stackManager.removeRune(element);
    }

    public override void PutSpell(ElementBehaviour element)
    {
        stackManager.putSpell(element);
    }

    public override void RemoveSpell(ElementBehaviour element)
    {
        stackManager.removeSpell(element);
    }

    public override void ExitMatch()
    {
        players[0].Lose();
    }
}
