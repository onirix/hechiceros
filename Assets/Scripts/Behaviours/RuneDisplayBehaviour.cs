﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneDisplayBehaviour : MonoBehaviour {

    [SerializeField]
    RuneData runeData;

    int index;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void SetRune(Transform parent, int rune)
    {
        transform.SetParent(parent);
        Image runeImage = GetComponent<Image>();
        runeImage.sprite = runeData.GetSprite(rune);
        /*switch (rune)
        {
            case 0:
                runeImage.color = Color.white;
                break;
            case 1:
                runeImage.color = Color.yellow;
                break;
            case 2:
                runeImage.color = Color.blue;
                break;
            case 3:
                runeImage.color = Color.green;
                break;
            case 4:
                runeImage.color = Color.red;
                break;
            case 5:
                runeImage.color = Color.gray;
                break;
            case 6:
                runeImage.color = Color.magenta;
                break;
            case 7:
                runeImage.color = Color.black;
                break;
        }*/
    }
}
