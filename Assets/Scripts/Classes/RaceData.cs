﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RaceData
{
    public int level = 1;

    public byte magic = 0;
    public byte afinity = 0;
    public byte weakness = 0;
    public byte strongest = 0;
}
