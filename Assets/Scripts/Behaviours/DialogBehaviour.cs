﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogBehaviour : MonoBehaviour {
    public delegate void PositiveResponse();
    public PositiveResponse positive;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisplayDialog(){
        gameObject.SetActive(true);
    }


    public void Positive(){
        positive();
    }
}
