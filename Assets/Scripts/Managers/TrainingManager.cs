﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingManager : GameManager {
    [SerializeField]
    FinishBehaviour finish;

    [SerializeField]
    TimerBehaviour timer;

    public Player player;


    public override void Init()
    {
        stackManager.isTraining = true;
        PlayerPrefs.SetInt("GameType", 0);
        player.InstantitePlayer(1, races[Random.Range(0, 8)],1);
        player.life = 100;
        int magicIndex = GetMagicIndex(player.race.magic);
        bgManager.SetBackground(magicIndex);
        musicManager.SetMusic(magicIndex);
        player.SetTurn();
        ResetRune();
    }
    public override int GetCurrentPlayerType()
    {
        return 0;
    }
    public override void setTurnAction(){
        Damage damage = stackManager.resolveSpell();
        int magic = damage.magic;
        int strongest = player.race.strongest;
        int afinity = player.race.afinity;
        comboManager.CheckCombo(magic, strongest, 0, afinity, damage.amoung);
        damage.amoung = 10 - damage.amoung;
        int life = player.TakeDamage(damage);
        player.playerManager.setLife(life);
        if (life > 0)
        {
            player.SetTurn();
        }
        else
        {
            finish.gameObject.SetActive(true);
            finish.FinishGame();
        }
        timer.ResetTimer();
        ResetRune();
    }
    private void ResetRune()
    {
        Rune startRune = new Rune(0);
        startRune.SetCriptoGram((byte)Random.Range(1, 256));
        stackManager.runeResult.SetMagicElement(startRune);
    }
    public override void PutRune(ElementBehaviour element)
    {
        stackManager.putRune(element);
    }

    public override void RemoveRune(ElementBehaviour element)
    {
        stackManager.removeRune(element);
    }

    public override void PutSpell(ElementBehaviour element)
    {
        stackManager.putSpell(element);
    }

    public override void RemoveSpell(ElementBehaviour element)
    {
        stackManager.removeSpell(element);
    }

    public override Player GetEnemyPlayer()
    {
        throw new System.NotImplementedException();
    }
    public override void ExitMatch()
    {
        sceneChanger.Previous(); ;
    }
}
