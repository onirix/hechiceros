﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {
    [SerializeField]
    SoundData musicData;

    [SerializeField]
    AudioSource audioSource;

   
    public void SetMusic(int i)
    {
        audioSource.clip = musicData.audios[i];
        audioSource.Play();
    }
}
