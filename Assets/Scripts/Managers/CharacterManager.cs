﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{

    SceneChanger sceneChanger;
    int next;
    // Use this for initialization
    void Awake()
    {
        sceneChanger = GetComponent<SceneChanger>();
        next = PlayerPrefs.GetInt("Next");
    }

    public void Next()
    {
        if (next == 0)//is a battle go to select enemy
        {
            sceneChanger.ChangeScene("HistoryCharacters");
        }
        else
        {
            sceneChanger.ChangeScene("History");
        }
    }
}
