﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitBehaviour : MonoBehaviour {
    Text message;
	// Use this for initialization
	void Awake () {
        message = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Hide(){
        message.enabled = false;;
    }
    public void Show(){
        message.enabled = true;
    }

}
