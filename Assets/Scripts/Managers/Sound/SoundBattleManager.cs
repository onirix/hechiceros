﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBattleManager : SoundManager
{
    delegate AudioClip DelegateSound();
    [SerializeField]
    SoundBattleData soundBattleData;

    public void CastSound()
    {
        audioSource.clip = soundBattleData.CastSound();
        audioSource.Play();
        Debug.Log("Cast PLayed");
    }
    public void CrashSound()
    {
        audioSource.clip = soundBattleData.CrashSound();
        audioSource.Play();
    }

    public void ComboSound()
    {
        DelegateSound delegateSound = soundBattleData.ComboSound;
        StartCoroutine(DelayedSound(1.5f,delegateSound));
    }
    private IEnumerator DelayedSound(float seconds, DelegateSound delegateSound)
    {
        yield return new WaitForSeconds(seconds);
        audioSource.clip = delegateSound();
        audioSource.Play();
        Debug.Log("Delayed PLayed");
    }
    public void DiedSound(int genere)
    {
        if (genere == 0)
        {
            audioSource.clip = soundBattleData.DiedMaleSound();
        }
        else
        {
            audioSource.clip = soundBattleData.DiedFemaleSound();
        }
        audioSource.Play();
    }
    public void DamageSound(int genere)
    {
        if (genere == 0)
        {
            audioSource.clip = soundBattleData.DamageMaleSound();
        }
        else
        {
            audioSource.clip = soundBattleData.DamageFemaleSound();
        }
        audioSource.Play();
    }
}
